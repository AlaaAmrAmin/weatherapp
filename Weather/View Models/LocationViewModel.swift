//
//  LocationViewModel.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

class LocationViewModel {
    
    private var locations = [Location]()
    private var weatherViewModel = WeatherViewModel()
    private var manager = LocationsManager()
    
    init() {
        locations = manager.locations()
    }
    
    func add(location: Location, completionHandler: @escaping (() -> Void)) {
        locations.append(location)
        manager.updateStorage(locations)
        weatherViewModel.getForcast(for: location.coordinates, isCurrentLocation: false, completionHandler: { weather in
            
            location.weather = weather
            completionHandler()
            
        }, errorHandler: nil)
    }
    
    func removeLocation(at index: Int) {
        if index < locations.count {
            locations.remove(at: index)
            manager.updateStorage(locations)
        }
    }
    
    func numberOfLocations() -> Int {
        return locations.count
    }
    
    func location(at index: Int) -> Location {
        return locations[index]
    }
    
    func updateLocationsWeather(completionHandler: @escaping ((Int) -> Void)) {
        for (i, location) in locations.enumerated() {
            weatherViewModel.getForcast(for: location.coordinates, isCurrentLocation: false, completionHandler: { [weak self] weather in
                
                location.weather = weather
                // In case of deleting a location while fetching its data
                if self != nil && i < self!.locations.count
                {
                    completionHandler(i)
                }
                
            }, errorHandler: nil)
        }
    }
}
