//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation
import CoreLocation

class WeatherViewModel {
    
    private var manager = WeatherManager()
    private var weather: Weather?
    
    func getForcast(for location: CLLocationCoordinate2D, isCurrentLocation: Bool, completionHandler: @escaping ((Weather) -> Void), errorHandler: ((String) -> Void)?) {
        
        var exclude = [WeatherManager.Exlude.flags.rawValue]
        if !isCurrentLocation
        {
            exclude.append(contentsOf: [WeatherManager.Exlude.currently.rawValue, WeatherManager.Exlude.hourly.rawValue])
        }
        
        manager.getForcast(for: location, without: exclude, completionHandler: { [weak self] weather in
            
            self?.weather = weather
            completionHandler(weather)
            
            }, errorHandler: { message in
                errorHandler?(message)
            })
    }
    
    func getWeather() -> Weather? {
        return weather
    }
    
    func currentTemperature() -> String {
       
        guard let weather = weather, let temperature = weather.currently?.temperature else { return "--" }
        
        return convertTemperatureToString(temperature)
    }
    
    func convertTemperatureToString(_ temperature: Double?) -> String {
        
        guard let temperature = convertFehrenheitToCelsius(temperature) else { return "--" }
        
        let temperatureString = String(format:"%.0f", temperature) + "°"
        return temperatureString
    }
    
    func convertFehrenheitToCelsius(_ temperature: Double?) -> Double? {
        
        guard let temperature = temperature else { return nil }
        
        return (temperature - 32) / 1.8
    }
}

// Hours Logic
extension WeatherViewModel {
    
    func numberOfHours() -> Int {
        
        guard let weather = weather, let hours = weather.hourly?.count else {
            return 0
        }
       return min(hours, 24)
    }
    
    func forcastForHour(at index: Int) -> Temperature {
        
        let hour = weather!.hourly![index]
        return hour
    }
    
    func hour(fromDate date: Date) -> String {
        
        let calendar = Calendar(identifier: .gregorian)
        var hour = calendar.component(.hour, from: date)
        var duration = "AM"
        
        if hour > 12
        {
            hour -= 12
            duration = "PM"
        }
        else if hour == 0
        {
            hour = 12
        }
        
        return String(hour) + duration
    }
    
}

// Days Logic
extension WeatherViewModel {
    
    func numberOfForcastingDays() -> Int {
        
        guard let weather = self.weather else { return 0 }
        let days = weather.daily
        return max(7, days.count)
    }
    
    func forcastForDay(at index: Int) -> Temperature {
        
        let day = weather!.daily[index]
        return day
    }
    
    func day(fromDate date: Date) -> String {
        
        let newDate = date.convertToTimeZone(from: TimeZone(abbreviation: "UTC")!, to: TimeZone.current)
        
        let calendar = Calendar(identifier: .gregorian)
        let weekDay = calendar.component(.weekday, from: newDate)
        let dayIndex = ((weekDay - 1) + (calendar.firstWeekday - 1)) % 7
        let weekDaySymbol = calendar.weekdaySymbols[dayIndex]
        
        return weekDaySymbol
    }
    
    func symbol(forTemperatureStatus status: Temperature.Status) -> String {
        
        var symbol = ""
        switch status {
        case .clearDay:
            symbol = "☀︎"
        case .clearNight:
            symbol = "☾"
        case .rain:
            symbol = "☂︎"
        case .snow:
            symbol = "☃︎"
        case .sleet, .wind, .fog, .cloudy, .partlyCloudyDay, .partlyCloudyNight:
            symbol = "☁︎"
        }
        return symbol
    }
}
