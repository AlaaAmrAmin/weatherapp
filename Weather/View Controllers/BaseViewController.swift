//
//  ViewController.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

// Show/Hide Loader
extension BaseViewController {
    
    func showLoader(withText text: String = "Loading", andDetailedText detailedText: String? = nil) {
        
        let loader = MBProgressHUD.showAdded(to: view, animated: true)
        loader.label.text = text
        loader.detailsLabel.text = detailedText
        loader.isUserInteractionEnabled = false
        loader.backgroundView.color = UIColor(named: Color.overlay.rawValue)!
    }
    
    func hideLoader() {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
}

