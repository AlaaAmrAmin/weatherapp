//
//  LocationsViewController.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationsViewController: BaseViewController {
    
    @IBOutlet weak var tableViewLocations: UITableView!
    @IBOutlet weak var viewPlaceholder: UIView!
    
    let viewModel = LocationViewModel()
    let weatherViewModel = WeatherViewModel()
    
    var updateWeather: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewLocations.register(UINib(nibName: NibName.weatherTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.weather.rawValue)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        
        if updateWeather
        {
            updateLocationsWeather()
        }
        else
        {
            updateWeather = true
        }
    }
    
    private func updateLocationsWeather() {
        viewModel.updateLocationsWeather { [weak self] index in
            self?.tableViewLocations.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    @IBAction func buttonAddLocations_TouchUpInside(_ sender: UIBarButtonItem) {
        updateWeather = false
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func buttonEditLocations_TouchUpInside(_ sender: UIBarButtonItem) {
        if tableViewLocations.isEditing == true
        {
            sender.title = "Edit"
        }
        else
        {
            sender.title = "Done"
        }
        
        tableViewLocations.isEditing = !tableViewLocations.isEditing
    }
    
}

extension LocationsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let number = viewModel.numberOfLocations()
        if number > 0
        {
            viewPlaceholder.isHidden = true
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.weather.rawValue, for: indexPath) as! WeatherTableViewCell
        
        let location = viewModel.location(at: indexPath.row)
        cell.labelTitle.text = location.name
        var icon = ""
        var maxTemperature = "--"
        var minTemperature = "--"
        if let weather = location.weather?.daily.first
        {
            icon = weatherViewModel.symbol(forTemperatureStatus: weather.status)
            maxTemperature = weatherViewModel.convertTemperatureToString(weather.maxTemperature)
            minTemperature = weatherViewModel.convertTemperatureToString(weather.minTemperature)
        }
        
        cell.labelIcon.text = icon
        cell.labelMaxTempreture.text = maxTemperature
        cell.labelLowTempreture.text = minTemperature
        
        return cell
    }
}

extension LocationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: StoryboardID.currentLocation.rawValue, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID.locationWeatherDetails.rawValue) as! LocationWeatherDetailsViewController
        vc.isUserLocation = false
        vc.location = viewModel.location(at: indexPath.row)
        navigationController?.pushViewController(vc, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            viewModel.removeLocation(at: indexPath.row)
            tableViewLocations.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

extension LocationsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let location = Location(id: place.placeID, name: place.name, coordinates: place.coordinate)
        let indexPath = IndexPath(row: viewModel.numberOfLocations(), section: 0)
        
        viewModel.add(location: location, completionHandler: { [weak self] in
            self?.tableViewLocations.reloadRows(at: [indexPath], with: .automatic)
        })
        
        dismiss(animated: true, completion: nil)
        
        tableViewLocations.insertRows(at: [indexPath], with: .automatic)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    
}
