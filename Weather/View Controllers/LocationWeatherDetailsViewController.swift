//
//  LocationWeatherDetailsViewController.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import UIKit
import CoreLocation

class LocationWeatherDetailsViewController: BaseViewController {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var labelLocationName: UILabel!
    @IBOutlet weak var labelTempreture: UILabel!
    @IBOutlet weak var collectionViewHours: UICollectionView!
    @IBOutlet weak var tableViewDays: UITableView!
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var labelPlaceholder: UILabel!
    
    let viewModel = WeatherViewModel()
    
    var location: Location?
    var isUserLocation: Bool = true
    var locationManager: CLLocationManager!
    
    var fetchingForcast: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewDays.register(UINib(nibName: NibName.weatherTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifier.weather.rawValue)
        
        if isUserLocation
        {
            locationManager = CLLocationManager()
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.delegate = self
        }
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        labelPlaceholder.text = "Loading data..."
        getForcast()
    }
   
    private func setupUI()
    {
        if navigationController == nil
        {
            buttonBack.isHidden = true
        }
        else
        {
            navigationController?.navigationBar.isHidden = true
            labelLocationName.text = location?.name
        }
    }
    
    private func updateUI()
    {
        labelTempreture.text = viewModel.currentTemperature()
        collectionViewHours.reloadData()
        tableViewDays.reloadData()
    }
    
    private func getForcast() {
        
        var coordinate: CLLocationCoordinate2D
        
        if isUserLocation
        {
            guard let currentLocation = locationManager.location?.coordinate else {
                return
            }
            coordinate = currentLocation
        }
        else
        {
            coordinate = location!.coordinates
        }

        if !fetchingForcast
        {
            fetchingForcast = true
            showLoader()
            viewModel.getForcast(for: coordinate, isCurrentLocation: true, completionHandler: { [weak self] _ in
                
                self?.fetchingForcast = false
                self?.updateUI()
                self?.hideLoader()
                
                }, errorHandler: { [weak self] message in
                
                    self?.fetchingForcast = false
                    self?.hideLoader()
                    self?.labelPlaceholder.text = message
                    if self != nil
                    {
                        UIHelper.alert(message: message, withTitle: "Ops", in: self!)
                    }
            })
        }
    }
    
    @IBAction func buttonBack_TouchUpInside() {
        navigationController?.popViewController(animated: true)
    }
    
}

extension LocationWeatherDetailsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let number = viewModel.numberOfHours()
        if number > 0
        {
            viewPlaceholder.isHidden = true
        }
        return number
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.hour.rawValue, for: indexPath) as! HourCollectionViewCell
        
        let hour = viewModel.forcastForHour(at: indexPath.item)
        cell.labelHour.text = viewModel.hour(fromDate: hour.time)
        var temperature = ""
        if indexPath.item == 0
        {
            temperature = viewModel.currentTemperature()
        }
        else
        {
            temperature = viewModel.convertTemperatureToString(hour.temperature)
        }
        cell.labelTempreture.text = temperature
        
        return cell
    }
    
}

extension LocationWeatherDetailsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfForcastingDays()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.weather.rawValue, for: indexPath) as! WeatherTableViewCell
        
        let day = viewModel.forcastForDay(at: indexPath.row)
        cell.labelTitle.text = viewModel.day(fromDate: day.time)
        cell.labelIcon.text = viewModel.symbol(forTemperatureStatus: day.status)
        cell.labelMaxTempreture.text = viewModel.convertTemperatureToString(day.maxTemperature)
        cell.labelLowTempreture.text = viewModel.convertTemperatureToString(day.minTemperature)
        
        return cell
    }
}

extension LocationWeatherDetailsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted
        {
            UIHelper.alert(message: "Please allow the app to access your location.", in: self)
            labelPlaceholder.text = "Can not access current location"
        }
        else
        {
            getForcast()
        }
        
    }
    
}
