//
//  HourCollectionViewCell.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import UIKit

class HourCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelTempreture: UILabel!
    
}
