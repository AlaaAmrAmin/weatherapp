//
//  WeatherManager.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation
import CoreLocation

struct WeatherManager {
    
    enum Exlude: String {
        case flags
        case currently
        case hourly
        case daily
    }
    
    func getForcast(for location: CLLocationCoordinate2D, without exclude: [String]? = nil, completionHandler: @escaping ((Weather) -> Void), errorHandler: @escaping ((String) -> Void)) {
        
        let location = "\(location.latitude),\(location.longitude)"
        var url = API.Weather.domain + API.Weather.Forcast.forcast + API.Weather.key + "/" + location
        
        if exclude != nil
        {
            url += "?exclude=[\(exclude!.joined(separator: ","))]"
        }
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .secondsSince1970
        
        NetworkManager.getRequest(withURL: url, usingDecoder: jsonDecoder, completionHandler: { (response: Safe<Weather>) in
            
            if let weather = Parser.parse(response: response)
            {
                completionHandler(weather)
            }
            else
            {
                errorHandler("An error has occured, please try again later")
            }
            
        }, errorHandler: errorHandler)
        
    }
    
}
