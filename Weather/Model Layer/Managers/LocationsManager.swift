//
//  LocationsManager.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/13/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

struct LocationsManager {
    
    func updateStorage(_ locations: [Location]) {
        do {
            if let path = try filePath()
            {
                let encoder = PropertyListEncoder()
                let data = try encoder.encode(locations)
                try data.write(to: path)
                
            }
        }
        catch FileError.notCreated(let message) {
            print(message)
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func locations() -> [Location] {
        
        do {
            if let path = try filePath()
            {
                let data = try Data(contentsOf: path)
                let decoder = PropertyListDecoder()
                let locations = try decoder.decode([Location].self, from: data)
                return locations
            }
        }
        catch FileError.notCreated(let message) {
            print(message)
        }
        catch {
            print(error.localizedDescription)
        }
        return [Location]()
    }
 
    private func filePath() throws -> URL? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths + "/Locations.plist"
        if !FileManager.default.fileExists(atPath: path)
        {
            do {
                if let bundle = Bundle.main.path(forResource: "Locations", ofType: "plist")
                {
                    let bundleURL = URL(fileURLWithPath: bundle)
                    let pathURL = URL(fileURLWithPath: path)
                    try! FileManager.default.copyItem(at: bundleURL, to: pathURL)
                }
                else
                {
                    throw FileError.notCreated("Faile to get bundle path")
                }
            }
            catch {
                throw FileError.notCreated("Failed to copy file")
            }
        }
        return URL(fileURLWithPath: path)
    }
    
    enum FileError: Error {
        case notCreated(String)
    }
}
