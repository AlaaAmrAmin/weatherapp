
//
//  Temperature.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

struct Temperature: Decodable {
    
    var time: Date
    var status: Status
    var temperature: Double?
    var maxTemperature: Double?
    var minTemperature: Double?
    
    enum Status: String, Decodable {
        case clearDay = "clear-day"
        case clearNight = "clear-night"
        case rain = "rain"
        case snow = "snow"
        case sleet = "sleet"
        case wind = "wind"
        case fog = "fog"
        case cloudy = "cloudy"
        case partlyCloudyDay = "partly-cloudy-day"
        case partlyCloudyNight = "partly-cloudy-night"
    }
    
   private enum CodingKeys: String, CodingKey {
        case time
        case status = "icon"
        case temperature
        case maxTemperature = "temperatureHigh"
        case minTemperature = "temperatureLow"
    }
}
