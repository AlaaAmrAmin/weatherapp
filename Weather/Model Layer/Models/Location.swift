//
//  Location.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation
import CoreLocation

class Location: Codable, Equatable {
    
    var id: String
    var name: String
    var coordinates: CLLocationCoordinate2D
    private var location: Coordinates
    var weather: Weather?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case location
    }
    
    struct Coordinates: Codable {
        var latitude: Double
        var longitude: Double
    }
    
    init(id: String, name: String, coordinates: CLLocationCoordinate2D) {
        self.id = id
        self.name = name
        self.coordinates = coordinates
        self.location = Coordinates(latitude: coordinates.latitude, longitude: coordinates.longitude)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        location = try container.decode(Coordinates.self, forKey: .location)
        coordinates = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(location, forKey: .location)
    }
    
    static func ==(lhs: Location, rhs: Location) -> Bool
    {
        if lhs.id == rhs.id
        {
            return true
        }
        return false
    }
}
