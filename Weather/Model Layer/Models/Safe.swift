//
//  Safe.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

// Reference: http://kean.github.io/post/codable-tips-and-tricks

struct Safe<Base:Decodable>: Decodable {
    
    var value: Base?
    
    init(from decoder: Decoder) throws {
        
        do {
            let container = try decoder.singleValueContainer()
            self.value = try container.decode(Base.self)
        } catch {
            assertionFailure("ERROR: \(error)")
            self.value = nil
        }
        
    }
}
