//
//  Weather.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

class Weather: Decodable {
    
    var currently: Temperature?
    var hourly: [Temperature]?
    var daily: [Temperature]
    
    private enum CodingKeys: String, CodingKey {
        case currently
        case hourly
        case daily
        case data
    }
    
    required init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.currently = try container.decodeIfPresent(Temperature.self, forKey: .currently)
        
        if let hourlyContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .hourly)
        {
            self.hourly = try hourlyContainer.decode([Temperature].self, forKey: .data)
        }
       
        let dailyContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .daily )
        self.daily = try dailyContainer.decode([Temperature].self, forKey: .data)
        
    }
    
}
