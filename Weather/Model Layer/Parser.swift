//
//  Parser.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

struct Parser {
    
    static func parse<T>(response: Safe<T>) -> T? {
        guard let value = response.value else
        {
            return nil
        }
        
        return value
    }
    
}
