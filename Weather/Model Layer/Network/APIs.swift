//
//  APIs.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

struct API {
    
    struct Weather {
        
        static let domain = "https://api.darksky.net/"
        static let key = "6790aeeaae76d9c1993e19931275be64"
        
        struct Forcast {
            static let forcast = "forecast/"
            
            struct Parameters {
                static let exclude = "exclude"
            }
        }
    }
    
    
    
    
}
