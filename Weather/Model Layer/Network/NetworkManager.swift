
//
//  NetworkManager.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkManager {
    
    static func getRequest<T: Decodable>(withURL url: String, usingDecoder decoder: JSONDecoder = JSONDecoder(), completionHandler: @escaping ((Safe<T>) -> Void), errorHandler: @escaping ((String) -> Void)) {
        
        guard let url = url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) else { return }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: [:]).validate().responseData{ response in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            switch response.result {
            case .success(let responseData):
                
                print("Get Request is Successful")
                
                do {
                    let data = try decoder.decode(Safe<T>.self, from: responseData)
                    completionHandler(data)
                }
                catch {
                    assertionFailure("An error has occured while parsing object\n \(error)")
                    errorHandler("An error has occured, please try again later")
                }
                
            case .failure(let error):

                if let error = error as? URLError, error.code == .notConnectedToInternet
                {
                    errorHandler("No internet connection")
                }
                else
                {
                    errorHandler(error.localizedDescription)
                }
            }
        }
        
    }
    
}
