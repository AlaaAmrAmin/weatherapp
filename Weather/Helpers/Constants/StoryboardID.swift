//
//  StoryboardID.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

enum StoryboardID: String {
    
    case main = "Main"
    case currentLocation = "CurrentLocation"
    case locations = "Locations"
    
}

enum NibName: String {
    
    case weatherTableViewCell = "WeatherTableViewCell"
}
