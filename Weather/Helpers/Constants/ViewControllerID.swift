
//
//  ViewControllerID'.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/11/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

enum ViewControllerID: String {
    
    case locationWeatherDetails = "LocationWeatherDetailsVC"
    case locations = "LocationsVC"
    
}
