//
//  Extensions.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation

extension Date {
    
    func convertToTimeZone(from fromTimeZone: TimeZone, to toTimeZone: TimeZone) -> Date {
        let delta = TimeInterval(toTimeZone.secondsFromGMT() - fromTimeZone.secondsFromGMT())
        return addingTimeInterval(delta)
   }
}
