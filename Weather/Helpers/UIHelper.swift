//
//  UIHelper.swift
//  Weather
//
//  Created by Alaa' Amr Amin on 11/12/18.
//  Copyright © 2018 Alaa' Amr Amin. All rights reserved.
//

import Foundation
import UIKit

struct UIHelper {
    
    static func alert(message: String, withTitle title: String? = nil, andButtonTitle buttonTitle: String = "OK", completionBlock: (() -> Void)? = nil, in viewController: UIViewController) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle , style: .default) { (_) in
            
            completionBlock?()
        }
        alertController.addAction(action)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
}
